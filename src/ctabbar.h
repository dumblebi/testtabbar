#ifndef CTABBAR_H
#define CTABBAR_H

#include <QTabBar>

class CTabBar : public QTabBar
{
public:
    explicit CTabBar(QWidget *parent = Q_NULLPTR);

protected:
    QSize tabSizeHint(int index) const;
    void paintEvent(QPaintEvent *);
};

#endif // CTABBAR_H
