#include "mainwindow.h"
#include <QGridLayout>
#include "ctabwidget.h"

#include <QPushButton>
#include <QToolButton>
#include <QApplication>
#include "canimatedicon.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setGeometry(100,100, 500,300);

    QWidget * widget = new QWidget;
    setCentralWidget(widget);

    QGridLayout * grid = new QGridLayout;
    widget->setLayout(grid);

    CTabWidget * _tabs = new CTabWidget;
    _tabs->setTabsClosable(true);
    _tabs->setMovable(true);
    _tabs->setIconSize(QSize(11,10));

    grid->addWidget(_tabs);

    _tabs->addTab(new QWidget, "Tab 1");
    _tabs->addTab(new QWidget, "Another Tab 2");

    QWidget * _w = _tabs->widget(0);
    m_icon = new CAnimatedIcon;
    _w->setLayout(new QGridLayout);
    _w->layout()->addWidget(m_icon);

    QPushButton * btn = new QPushButton("Start");
    btn->setFixedSize(80, 27);
    _w->layout()->addWidget(btn);
    connect(btn, &QPushButton::clicked, this, &MainWindow::onBtnStart);

    btn = new QPushButton("Next");
    btn->setFixedSize(80, 27);
    _w->layout()->addWidget(btn);
    connect(btn, &QPushButton::clicked, this, &MainWindow::onBtnNext);

    QToolButton * toolbtn = new QToolButton;
    _w->layout()->addWidget(toolbtn);

    _tabs->setTabIcon(0, QIcon(":/icon1.png"));
    _tabs->setTabIcon(1, QIcon(":/icon2.png"));

    qApp->setStyleSheet("QLabel{background-color:#f00}"
                    "QToolButton{background-color:#ff0}"
                    "QTabBar QToolButton{background-color:#f00}");
}

MainWindow::~MainWindow()
{

}

void MainWindow::onBtnStart()
{
    ((CAnimatedIcon *)m_icon)->startSvg(":/res/loader.svg", "bluecircle");
}

void MainWindow::onBtnNext()
{
    ((CAnimatedIcon *)m_icon)->setSvgElement("redcircle");
}
