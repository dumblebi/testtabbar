#ifndef CANIMATEDICON_H
#define CANIMATEDICON_H

#include <QLabel>
#include <QSvgRenderer>

class CAnimatedIcon : public QLabel
{
public:
    explicit CAnimatedIcon();

    void setPixmap(const QPixmap &);

    void startSvg(const QString&, const QString& id = QString());
    void setSvgElement(const QString&);

private:
    QSvgRenderer * m_svg = nullptr;
    QString m_svgElemId;
    QPixmap * m_image = nullptr;

private slots:
    void onSvgRepaint();
};

#endif // CANIMATEDICON_H
