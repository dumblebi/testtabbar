#ifndef CTABBARSTYLE_H
#define CTABBARSTYLE_H

#include <QProxyStyle>

class CTabBarStyle : public QProxyStyle
{
public:
//    explicit CCustomStyle(QStyle *style = Q_NULLPTR);

    void drawPrimitive(PrimitiveElement element, const QStyleOption *option, QPainter *painter, const QWidget *widget = Q_NULLPTR) const;
    void drawControl(ControlElement element, const QStyleOption *option, QPainter *painter, const QWidget *widget = Q_NULLPTR) const;
    int pixelMetric(PixelMetric metric, const QStyleOption *option = Q_NULLPTR, const QWidget *widget = Q_NULLPTR) const;

//    SubControl hitTestComplexControl(ComplexControl control, const QStyleOptionComplex *option, const QPoint &pos, const QWidget *widget = Q_NULLPTR) const;
    int styleHint(StyleHint hint, const QStyleOption *option = Q_NULLPTR, const QWidget *widget = Q_NULLPTR, QStyleHintReturn *returnData = Q_NULLPTR) const;
    void drawItemText(QPainter *painter, const QRect &rect, int flags, const QPalette &pal, bool enabled,
                            const QString &text, QPalette::ColorRole textRole = QPalette::NoRole) const;

    QRect subElementRect(SubElement element, const QStyleOption *option, const QWidget *widget) const;
//    QRect itemPixmapRect(const QRect &r, int flags, const QPixmap &pixmap) const;
//    QRect subControlRect(ComplexControl cc, const QStyleOptionComplex *opt, SubControl sc, const QWidget *w = Q_NULLPTR) const;
};

#endif // CTABBARSTYLE_H
