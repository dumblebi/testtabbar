#include "ctabbarstyle.h"
#include <QStyleOptionTab>
#include <QDebug>
#include <QPainter>


//CTabBarStyle::CTabBarStyle(QStyle *style)
//    : QProxyStyle(style)
//{

//}

void CTabBarStyle::drawPrimitive(PrimitiveElement element, const QStyleOption *option, QPainter *painter, const QWidget *widget) const
{
//    qDebug() << "drawPrimitive: " << element;
    QProxyStyle::drawPrimitive(element, option, painter, widget);
}

void CTabBarStyle::drawControl(ControlElement element, const QStyleOption *option, QPainter *painter, const QWidget *widget) const
{
    if (element == CE_TabBarTab) {
        if (const QStyleOptionTab *tab = qstyleoption_cast<const QStyleOptionTab *>(option)) {
            QStyleOptionTab opt(*tab);

//            painter->setBrush(QColor("#f00"));
            QProxyStyle::drawControl(element, &opt, painter, widget);
            return;
        }
    } else
    if (element == CE_TabBarTabLabel) {
        if (const QStyleOptionTab *tab = qstyleoption_cast<const QStyleOptionTab *>(option)) {
            QStyleOptionTab opt(*tab);
    //            opt.shape = QTabBar::RoundedWest;

            QProxyStyle::drawControl(element, &opt, painter, widget);
            return;
        }
    } else
    if (element == CE_TabBarTabShape) {
        if (const QStyleOptionTab *tab = qstyleoption_cast<const QStyleOptionTab *>(option)) {
            QStyleOptionTab opt(*tab);
qDebug() << "palette before: " << opt.palette.base();
            opt.palette.setBrush(QPalette::Base, QBrush(QColor("#f00")));
qDebug() << "palette: " << opt.shape;

            QProxyStyle::drawControl(element, &opt, painter, widget);
            return;
        }
    }


    qDebug() << "drawControl: " << element;
    QProxyStyle::drawControl(element, option, painter, widget);
}

int CTabBarStyle::pixelMetric(PixelMetric metric, const QStyleOption *option, const QWidget *widget) const
{
    if ( metric == PM_TabBarTabHSpace ) {
//        return 150;
    } else
    if ( metric == PM_TabBarTabVSpace ) {
//        return 10;
    } else
    if ( metric == PM_TabBarTabShiftVertical ) {
        // shift inner content down
        return 0;
    } else
    if ( metric == PM_TabBarTabOverlap ) {
        // shift tab down in relation to current one
        return 0;
    }

//    qDebug() << "pixelMetric: " << metric << ", " << QProxyStyle::pixelMetric(metric, option, widget);
    return QProxyStyle::pixelMetric(metric, option, widget);
}

//QStyle::SubControl CTabBarStyle::hitTestComplexControl(ComplexControl control, const QStyleOptionComplex *option, const QPoint &pos, const QWidget *widget) const
//{
//    qDebug() << "hitTestComplexControl: " << control;

//    return QProxyStyle::hitTestComplexControl(control, option, pos, widget);
//}

int CTabBarStyle::styleHint(StyleHint hint, const QStyleOption *option, const QWidget *widget, QStyleHintReturn *returnData) const
{
//    qDebug() << "style hint: " << hint;

    return QProxyStyle::styleHint(hint, option, widget, returnData);
}

void CTabBarStyle::drawItemText(QPainter *painter, const QRect &rect, int flags, const QPalette &pal,
                                    bool enabled, const QString &text, QPalette::ColorRole textRole) const
{
    flags = (flags & ~Qt::AlignCenter) | Qt::AlignLeft | Qt::AlignVCenter;
    QProxyStyle::drawItemText(painter, rect, flags, pal, enabled, text, textRole);
}

QRect CTabBarStyle::subElementRect(SubElement element, const QStyleOption *option, const QWidget *widget) const
{
//    qDebug() << "subelement rect: " << element << "," << QProxyStyle::subElementRect(element, option, widget);

    if ( element == SE_TabBarTabLeftButton ) {
        qDebug() << "icon rect";
    } else
    if ( element == SE_TabBarTabRightButton ) {
        QRect _rc = QProxyStyle::subElementRect(element, option, widget);
//        _rc.moveRight(10);
        return _rc;
    }

    return QProxyStyle::subElementRect(element, option, widget);
}

//QRect CTabBarStyle::itemPixmapRect(const QRect &r, int flags, const QPixmap &pixmap) const
//{
//    qDebug() << "pixmap rect: ";
//    return QRect();
//}

//QRect CTabBarStyle::subControlRect(QStyle::ComplexControl cc, const QStyleOptionComplex *opt, QStyle::SubControl sc, const QWidget *w) const
//{
//    qDebug() << "sub control rect: ";
//    return QProxyStyle::subControlRect(cc, opt, sc, w);
//}
