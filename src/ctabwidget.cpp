#include "ctabwidget.h"
#include "ctabbar.h"
#include "ctabbarstyle.h"
#include "ctabwidgetstyle.h"

#include <QPushButton>

CTabWidget::CTabWidget(QWidget * parent)
    : QTabWidget(parent)
{
    CTabBar * bar = new CTabBar;
    bar->setStyle(new CTabBarStyle);
    setTabBar(bar);

//    setCornerWidget(new QPushButton("START"), Qt::TopLeftCorner);
    setStyle(new CTabWidgetStyle);
}
