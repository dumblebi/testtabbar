#include "ctabwidgetstyle.h"
#include <QDebug>

CTabWidgetStyle::CTabWidgetStyle()
    : QProxyStyle()
{
}

QRect CTabWidgetStyle::subElementRect(SubElement element, const QStyleOption *option, const QWidget *widget) const
{
//    qDebug() << "subelement rect: " << element << "," << QProxyStyle::subElementRect(element, option, widget);

    if ( element == SE_TabWidgetTabBar ) {
        QRect _rect = QProxyStyle::subElementRect(element, option, widget);
        _rect.moveLeft(50);
        return _rect;
    }

    return QProxyStyle::subElementRect(element, option, widget);
}

int CTabWidgetStyle::styleHint(StyleHint hint, const QStyleOption *option, const QWidget *widget, QStyleHintReturn *returnData) const
{
//    qDebug() << "style hint: " << hint;
//    if ( hint == SH_TabBar_Alignment )
//        return Qt::AlignRight;

    return QProxyStyle::styleHint(hint, option, widget, returnData);
}
