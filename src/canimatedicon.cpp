#include "canimatedicon.h"

#include <QPainter>

CAnimatedIcon::CAnimatedIcon()
    : QLabel()
{

}

void CAnimatedIcon::setPixmap(const QPixmap & pixmap)
{
    if ( m_svg && m_svg->animated() ) {
        disconnect(m_svg, &QSvgRenderer::repaintNeeded, this, &CAnimatedIcon::onSvgRepaint);
        if  ( m_image ) delete m_image, m_image = nullptr;
    }

    QLabel::setPixmap(pixmap);
}

void CAnimatedIcon::startSvg(const QString& source, const QString& id)
{
    if ( !m_svg ) m_svg = new QSvgRenderer(this);
    else disconnect(m_svg);

    if ( m_svg->load(source) ) {
        setFixedSize( m_svg->defaultSize() );

        if ( m_svg->animated() ) {
            m_svgElemId = id;

            if ( m_image ) delete m_image;
            m_image = new QPixmap(m_svg->defaultSize());

            connect(m_svg, &QSvgRenderer::repaintNeeded, this, &CAnimatedIcon::onSvgRepaint);
        } else {
            QPixmap image( m_svg->defaultSize() );
            QPainter painter( &image );

            if ( id.isEmpty() )
                m_svg->render( &painter );
            else m_svg->render( &painter, id );

            QLabel::setPixmap( image );
        }
    }
}

void CAnimatedIcon::onSvgRepaint()
{
    m_image->fill(Qt::transparent);
    QPainter painter( m_image );

    if ( m_svgElemId.isEmpty() )
        m_svg->render( &painter );
    else m_svg->render( &painter, m_svgElemId );

    QLabel::setPixmap( *m_image );
}

void CAnimatedIcon::setSvgElement(const QString& id)
{
    m_svgElemId = id;
}
