#include "ctabbar.h"
#include "ctabbarstyle.h"
#include <QDebug>

CTabBar::CTabBar(QWidget *parent)
    : QTabBar(parent)
{
//    setDrawBase(false);
//    QTabBar::setStyle(new CCustomStyle);   
}

QSize CTabBar::tabSizeHint(int index) const
{
//    QSize _size = QTabBar::tabSizeHint(index);
//    _size.setWidth(150);
    return QSize(150,28);
}

void CTabBar::paintEvent(QPaintEvent * e)
{
    qDebug() << "paint event";
    QTabBar::paintEvent(e);
}
