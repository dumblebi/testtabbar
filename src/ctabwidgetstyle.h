#ifndef CTABWIDGETSTYLE_H
#define CTABWIDGETSTYLE_H

#include <QProxyStyle>

class CTabWidgetStyle : public QProxyStyle
{
public:
    CTabWidgetStyle();

    QRect subElementRect(SubElement element, const QStyleOption *option, const QWidget *widget) const;
    int styleHint(StyleHint hint, const QStyleOption *option = Q_NULLPTR, const QWidget *widget = Q_NULLPTR, QStyleHintReturn *returnData = Q_NULLPTR) const;
};

#endif // CTABWIDGETSTYLE_H
