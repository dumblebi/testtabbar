#-------------------------------------------------
#
# Project created by QtCreator 2018-02-12T23:49:59
#
#-------------------------------------------------

QT       += core gui
QT       += svg

CONFIG  += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = testTabbar
TEMPLATE = app


SOURCES += src\main.cpp\
        src\mainwindow.cpp \
        src\ctabwidget.cpp \
    src/ctabbar.cpp \
    src/canimatedicon.cpp \
    src/ctabwidgetstyle.cpp \
    src/ctabbarstyle.cpp

HEADERS  += src\mainwindow.h \
            src\ctabwidget.h \
    src/ctabbar.h \
    src/canimatedicon.h \
    src/ctabwidgetstyle.h \
    src/ctabbarstyle.h

RESOURCES += $$PWD/resources.qrc
